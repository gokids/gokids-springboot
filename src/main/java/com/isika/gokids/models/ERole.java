package com.isika.gokids.models;

public enum ERole {
	ROLE_USER,
    ROLE_ADMIN
}
